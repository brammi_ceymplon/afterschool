<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\Actionable;

class Center extends Model
{
    use Actionable;

    public function notes(){
        return $this->hasMany(Note::class);
    }

}
