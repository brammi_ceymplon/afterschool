<?php

namespace App;

use Laravel\Nova\Actions\Actionable;
use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    use Actionable;

    public function kid(){
        return $this->belongsTo(Kid::class);
    }
    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}
