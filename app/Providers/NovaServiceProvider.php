<?php

namespace App\Providers;
use Acme\PriceTracker\PriceTracker;
use Acme\StripeInspector\StripeInspector;
use Laravel\Nova\Fields\ID;
use Illuminate\Support\Facades\Request;
use App\Nova\Metrics\AttendencePerDay;
use App\Nova\Metrics\KidsPerCenter;
use App\Nova\Metrics\NewContacts;
use App\Nova\Metrics\NewKids;
use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Beyondcode\CustomDashboardCard\NovaCustomDashboard;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        NovaCustomDashboard::cards([
            new KidsPerCenter,
            new NewContacts,
            new NewKids,
            new AttendencePerDay(),
            // ... all cards you want to be available
        ]);
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new \Beyondcode\CustomDashboardCard\CustomDashboard,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            (new PriceTracker)->canSee(function ($request) {
                return true;
            }),

        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),

            StripeInspector::make()->canSee(function ($request) {
                return true;
            }),
        ];
    }
}