<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\Actionable;

class Contact extends Model
{
    use Actionable;

    public function kids(){
        return $this->hasMany(Kid::class);
    }
}
