<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\Actionable;

class Kid extends Model
{
    use Actionable;

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function center()
    {
        return $this->belongsTo(Center::class);
    }

    public function register(){
        return $this->hasOne(Register::class);
    }
}

