<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\LensRequest;

class MostCrowdedCenter extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->select(self::columns())
                ->join('kids','centers.id','=','kids.center_id')
                ->orderBy('total_kids','dsc')
                ->groupBy('centers.id', 'centers.name')
        ));
    }
    protected static function columns()
    {
        return [
            'centers.id',
            'centers.name',
            DB::raw('count(centers.id) as total_kids'),
        ];
    }


    public function fields(Request $request)
    {
        return [
            ID::make('ID', 'id')->sortable(),
            Text::make('Name')->sortable(),
            Number::make('Total Kids', 'total_kids', function ($value) {
                return $value;}),


        ];
    }
    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'most-crowded-center';
    }
}
