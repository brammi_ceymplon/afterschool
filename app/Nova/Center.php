<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Http\Requests\NovaRequest;

class Center extends Resource
{

    public static $model = 'App\Center';


    public static $title = 'name';


    public static $search = [
        'id','name',
    ];


    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),
            Place::make('Address')->hideFromIndex(),
            Number::make('phone'),

        ];
    }


    public function cards(Request $request)
    {
        return [
            //new Metrics\KidsPerDay(),
        ];
    }


    public function filters(Request $request)
    {
        return [ ];
    }


    public function lenses(Request $request)
    {
        return [new Lenses\MostCrowdedCenter()

        ];
    }


    public function actions(Request $request)
    {
        return [];
    }
}
