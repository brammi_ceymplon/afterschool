<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Note extends Resource
{

    public static $model = 'App\Note';


    public static $title = 'name';


    public static $search = [
        'id','name'
    ];


    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Textarea::make('Message'),
            BelongsTo::make('Center')->sortable(),
            DateTime::make('created at'),


        ];
    }


    public function cards(Request $request)
    {
        return [];
    }


    public function filters(Request $request)
    {
        return [ new Filters\ByCenter()];
    }


    public function lenses(Request $request)
    {
        return [];
    }


    public function actions(Request $request)
    {
        return [];
    }
}
