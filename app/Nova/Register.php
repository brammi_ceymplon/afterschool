<?php

namespace App\Nova;
use Illuminate\Validation;
use App\Center;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\ID;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Register extends Resource
{

    public static $model = 'App\Register';


    public static $title = 'id';


    public static $search = [
        'id',
    ];


    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Kid'),

            Boolean::make('Movement')
                ->trueValue('In')
                ->falseValue('Out'),




        ];

    }


    public function cards(Request $request)
    {
        return [new Metrics\AttendencePerDay()];
    }


    public function filters(Request $request)
    {
        return [new Filters\MovementType(),
        new Filters\ByCenter()
        ];
    }


    public function lenses(Request $request)
    {
        return [];
    }


    public function actions(Request $request)
    {
        return [];
    }
}
