<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
//use App\Nova\Filters\CentreType;
//use App\Nova\Filters\ByContact;
use Laravel\Nova\Http\Requests\NovaRequest;

class Kid extends Resource
{

    public static $model = 'App\Kid';



    public function title()
    {
        return $this->name;
    }

    public static $search = [
        'id','name',
    ];


    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name')->sortable(),
            BelongsTo::make('Contact')->searchable(),
            BelongsTo::make('Center')->searchable(),
        ];
    }


    public function cards(Request $request)
    {
        return [
            new Metrics\NewKids(),
           new Metrics\KidsPerCenter(),
        ];
    }


    public function filters(Request $request)
    {
        return [
          new Filters\ByCenter(),
           new Filters\ByContact(),
        ];
    }


    public function lenses(Request $request)
    {
        return [
            //new Lenses\MostCrowdedCentre(),
        ];
    }


    public function actions(Request $request)
    {
        return [];
    }
}
