<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\Actionable;

class Note extends Model
{
    use Actionable;

    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}
